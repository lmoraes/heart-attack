import sys
import pickle
import pandas as pd
import numpy as np
from copy import deepcopy
from sklearn.metrics import roc_curve, auc, classification_report, accuracy_score, recall_score
import matplotlib.pyplot as plt

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx

def risco_cardiaco():
    if len(sys.argv) == 1:
        print("Uso: python risco_cardiaco.py <nome do arquivo>")
    else:
        filename = sys.argv[1]

    file_ext = filename.split(".")[-1]

    if file_ext == "xls" or file_ext == "xlsx":
        # Open xlsx file
        with open(filename, 'rb') as xls_file:
            dataset = pd.read_excel(xls_file)

        try:
            dataset.drop('Unnamed: 4', axis=1, inplace=True)
            dataset.drop('Unnamed: 5', axis=1, inplace=True)
        except ValueError:
            pass
    if file_ext == "csv" or file_ext == "txt":
        # Open CSV file
        with open(filename, 'rb') as xls_file:
            dataset = pd.read_csv(xls_file, names=["Colesterol Total", "Idade", "Glicemia", "Desfecho"])

    # Get separated input and output
    X = dataset.drop('Desfecho', axis=1)
    Y = dataset[['Desfecho']].as_matrix().T[0]

    # Open model
    with open("model.pkl", "rb") as pkl_file:
        model = pickle.load(pkl_file)

    # Apply data transformation
    X_scaled = (X - model["mean"]) / model["std"]

    # Predict probabilities
    probabilities = model["model"].predict_proba(X_scaled)
    probabilities = probabilities[:, 1]

    # Classifica os dados de acordo com o ponto de corte
    Y_predicted = deepcopy(probabilities)
    Y_predicted[Y_predicted > model["threshold"]] = 1
    Y_predicted[Y_predicted <= model["threshold"]] = -1

    # Resultados
    print("Aperte qualquer tecla para exibir o resultado na tela...")
    input()
    print(Y_predicted)

    # Calcula curva ROC
    fpr, tpr, thresholds = roc_curve(Y, probabilities)
    roc_auc = auc(fpr, tpr)
    print("Criando curva ROC...\n\n\n")
    print("Área da curva ROC: %.2f\n\n\n" % roc_auc)

    # Plota curva ROC
    plt.figure()
    plt.plot(fpr, tpr, lw=1, label='Curva ROC (área = %0.2f)' % roc_auc)

    best_prob = find_nearest(thresholds, model["threshold"])
    plt.plot(fpr[best_prob], tpr[best_prob], 'or', label="Corte %.2f" % model["threshold"])

    plt.annotate(
        "%.2f" % model["threshold"],
        xy = (fpr[best_prob], tpr[best_prob]), xytext = (-20, 20),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Aleatório')

    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('Taxa de falsos positivos')
    plt.ylabel('Taxa de verdadeiros positivos')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right", prop={"size":9})

    plt.savefig("curva_roc.png")
    print("Curva ROC criada!")
    print("Abra o arquivo curva_roc.png para visualizar a curva ROC.\n\n\n")

    # Calcula métricas
    print(classification_report(Y, Y_predicted))
    print("Acurácia dada pela curva ROC: %.2f" % roc_auc)
    recall_0 = recall_score(Y, Y_predicted, pos_label=-1)
    recall_1 = recall_score(Y, Y_predicted, pos_label=1)
    print("Acurácia balanceando por classe (Recall(-1) * 0.5 + Recall(1) * 0.5): %.2f\n\n\n" % (recall_0 * 0.5 + recall_1 * 0.5))
    # print("Acurácia dada pela conta com todas as observações: %.2f" % accuracy_score(Y, Y_predicted))

    print("Salvando resultado em arquivo...")
    with open("predicted.csv", "wb") as csv_file:
        np.savetxt(csv_file, Y_predicted.astype(int))
    print("Abra o arquivo predicted.csv para ver o resultado.")


if __name__ == "__main__":
    risco_cardiaco()
